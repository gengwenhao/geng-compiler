const compiler = require('../compiler')

function compilerMixin(GengCompiler) {
  GengCompiler.prototype = Object.assign(GengCompiler.prototype, compiler)
}

module.exports = {
  compilerMixin
}
