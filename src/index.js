const {compilerMixin} = require('./mixins')

const packageInfo = require('../package.json')

function GengCompiler() {
  const privateFields = [
    'name',
    'description',
    'version',
    'author',
    'license'
  ]

  privateFields.forEach(fields => {
    this['__' + fields + '__'] = packageInfo[fields] ?? ''
  })
}

compilerMixin(GengCompiler)

module.exports = GengCompiler
