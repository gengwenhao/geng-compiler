function lexer(code) {
  return code.split(/\s+/)
             .filter(s => s)
             .map(s => ({type: isNaN(s) ? 'word' : 'number', value: s}))
}

function parser(tokens) {
  const ast = {
    type: 'Drawing',
    body: []
  }

  while (tokens.length) {
    const curToken = tokens.shift()

    if (curToken.type === 'word') {
      switch (curToken.value) {
        case 'Paper':
          const expression = {
            type: 'CallExpression',
            name: 'Paper',
            arguments: []
          }

          const argument = tokens.shift()
          if (argument.type === 'number') {
            expression.arguments.push({
              type: 'NumberLiteral',
              value: argument.value
            })
            ast.body.push(expression)
          } else {
            throw 'Paper command must be followed by a number.'
          }
          break
      }
    }
  }

  return ast
}

module.exports = {
  lexer,
  parser,
}
