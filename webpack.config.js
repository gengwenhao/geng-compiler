const path = require('path')
const nodeExternals = require('webpack-node-externals')

module.exports = {
  externals: [nodeExternals({})],
  entry: {
    'api': './src/index.js',
    'cli': './cli/index.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    library: 'GengCompiler',
    // libraryExport: 'default',
    libraryTarget: 'umd'
  },
  target: 'node',
  mode: 'production'
}
