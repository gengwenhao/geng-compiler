const GengCompiler = require('../dist/api')

const compiler = new GengCompiler()

const tokens = compiler.lexer('Paper 100')
console.log(JSON.parse(JSON.stringify(tokens)))

const ast = compiler.parser(tokens)
console.log(ast)
